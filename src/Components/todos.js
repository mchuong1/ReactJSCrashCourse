import React, { Component } from 'react';
import TodoItem from './todo.item';
import PropTypes from 'prop-types'; //used for validation
class Todo extends Component {
    deleteProject(id){
        this.props.onDelete(id);
    }
  render() {
    let todoItems;
    if(this.props.todos){
        todoItems = this.props.todos.map(todo => {
            return (
                <TodoItem 
                key={todo.title} todo={todo} />
            );
        });
    }
      //console.log(this.props);
    return (
      <div className="Todos">
        Todos List
        <hr></hr>
        {todoItems}
      </div>
    );
  }
}

//used this for validation
Todo.propTypes = { 
    todos: PropTypes.array
}

export default Todo;