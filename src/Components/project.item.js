import React, { Component } from 'react';
import PropTypes from 'prop-types';
class ProjectItem extends Component {
    deleteProject(id){
        //pass it to main component and delete there
        this.props.onDelete(id);
    }
  render() {
    return (
        //Can't use class in JSX use className
        //have to use html for can't use for
      <li className="ProjectItem">
        <strong>{this.props.project.id}</strong> <br/>
        <strong>{this.props.project.title}</strong> - {this.props.project.category} 
        <button onClick={this.deleteProject.bind(this, this.props.project.id)}>X</button>
        <hr></hr>
      </li>
    );
  }
}
ProjectItem.propTypes = { 
    project: PropTypes.object,
    onDelete: PropTypes.func
}

export default ProjectItem;