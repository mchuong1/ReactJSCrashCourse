import React, { Component } from 'react';
import uuid from 'uuid';
import PropTypes from 'prop-types';
//State in this Component is different from the state in the main app component
class addProject extends Component {
    constructor(){
        super();//always call super when making constructor
        this.state = {
            newProject: {}
        }
    }

    static defaultProps = {
        categories: ['Web Design', 'Unreal Engine', 'Youtube']
    }

    handleSubmit(e){
        if(this.refs.title.value === ''){
            alert('Title is required!');
        } else {
            this.setState({newProject:{
                id: uuid.v4(),
                title: this.refs.title.value,
                category: this.refs.category.value
            }}, function(){
               // Passing up to main app.js
               this.props.addProject(this.state.newProject);
            });
        }
        e.preventDefault();
    }
  render() {
      //console.log(this.props);
      let categoryOptions = this.props.categories.map(category => {
        return <option key={category} value={category}>{category}</option>
    });
    return (
      <div>
        <h3>Add Projects</h3>
        <form onSubmit={this.handleSubmit.bind(this)}>
            <div>
                <label>Title</label><br/>
                <input type="text" ref="title"/>
            </div>
            <div>
                <label>Category</label><br/>
                <select ref="category">
                    {categoryOptions}
                </select>
            </div>
            <input type="submit" value="Submit" />
        </form>
      </div>
    );
  }
}

addProject.propTypes = { 
    categories: PropTypes.array,
    addProject: PropTypes.func,
}

export default addProject;