import React, { Component } from 'react';
import PropTypes from 'prop-types';

class TodoItem extends Component {
    
  render() {
    return (
        //Can't use class in JSX use className
        //have to use html for can't use for
      <li className="Todo">
        <strong>{this.props.todo.title}</strong>
      </li>
    );
  }
}
TodoItem.propTypes = { 
    todo: PropTypes.object
}

export default TodoItem;