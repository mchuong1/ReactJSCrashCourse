import React, { Component } from 'react';
import ProjectItem from './project.item';
import PropTypes from 'prop-types'; //used for validation
class Projects extends Component {
    deleteProject(id){
        this.props.onDelete(id);
    }
  render() {
    let projectItems;
    if(this.props.projects){
        projectItems = this.props.projects.map(project => {
            return (
                <ProjectItem 
                onDelete={this.deleteProject.bind(this)} 
                key={project.title} 
                project={project} />
            );
        });
    }
      //console.log(this.props);
    return (
      <div className="Projects">
        My Projects
        <hr></hr>
        {projectItems}
      </div>
    );
  }
}

//used this for validation
Projects.propTypes = { 
    projects: PropTypes.array,
    onDelete: PropTypes.func
}

export default Projects;