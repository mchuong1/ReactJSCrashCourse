import React, { Component } from 'react';
import Projects from './Components/projects';
import AddProject from './Components/addProject';
import Todos from './Components/todos';
import uuid from 'uuid';
import $ from 'jquery';
import './App.css';
//Import with Uppercase

class App extends Component {
  constructor() {
    super();
    this.state = {
      projects: [],
      todos: []
    }
  }

  //want to keep data here in this lifecycle method
  componentWillMount(){
    this.getProjects();
    this.getTodos();
  }

  componentDidMount(){
    this.getTodos();
  }

  //regular methods
  getProjects(){
    this.setState({projects: [
      {
        id: uuid.v4(),
        title: 'Business Website',
        category: 'Web Design'
      },
      {
        id: uuid.v4(),
        title: 'Gaming App',
        category: 'Unreal Engine'
      },
      {
        id: uuid.v4(),
        title: 'Tucker\'s MixTapes',
        category: 'Youtube'
      }
    ]});
  }

  getTodos(){
    $.ajax({
      url:'https://jsonplaceholder.typicode.com/todos',
      dataType: 'json',
      cache: false,
      success: function(data){
        this.setState({todos: data}, function(){
          console.log(this.state);
        })
      }.bind(this),
      error: function(xhr, status, err){
        console.log(err);
      }
    })
  }

  handleAddProject(project) {
    //want to add it to the main app.js state
    let projects = this.state.projects;
    projects.push(project);
    this.setState({projects:projects});
  }

  handleDeleteProject(id) {
    let projects = this.state.projects;
    let index = projects.findIndex(x => x.id === id);
    projects.splice(index, 1);
    this.setState({projects:projects});
  }

  render() {
    return (
      <div className="App">
        <AddProject addProject={this.handleAddProject.bind(this)}/>
        <br/>
        <Projects projects={this.state.projects} onDelete={this.handleDeleteProject.bind(this)}/>
        <hr></hr>
        <Todos todos={this.state.todos}/>
      </div>
    );
  }   //Passing the state.projects into Projects.js file
}

export default App;
